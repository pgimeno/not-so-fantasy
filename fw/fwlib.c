/*
 * RetroAdvance framework for GBA - C side
 *
 * (C) Copyright 2021 Pedro Gimeno Fortea
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include <string.h>
#include <stdbool.h>
#include <stdarg.h>
#include <acsl.h>
#include <stdio.h>
#ifndef RT_EXCEPTION
#define RT_EXCEPTION 1
#endif
#include "fwlib.h"
typedef u16 tBGVRAM[4][1024];
// GBA hardware
static u16   *const PALRAM  = (void *)0x05000000;
static u16   *const VRAM    = (void *)0x06000000;
static u16   *const OAM     = (void *)0x07000000;
static tBGVRAM *const BGVRAM = (void *)0x06000000;
// Memory mapped I/O registers
static u16   *const REG_DISPCNT  = (void *)0x04000000;
static u16   *const REG_DISPSTAT = (void *)0x04000004;
static u16   *const REG_BGCNT    = (void *)0x04000008;
static u16   *const REG_BGOFS    = (void *)0x04000010;
static u16   *const REG_WINH     = (void *)0x04000040;
static u16   *const REG_WINV     = (void *)0x04000044;
static u16   *const REG_WININ    = (void *)0x04000048;
static u16   *const REG_WINOUT   = (void *)0x0400004A;
// Interrupt stuff
static u16   *const REG_IME = (void *)0x04000208;
static u16   *const REG_IE  = (void *)0x04000200;
static u16   *const REG_IF  = (void *)0x04000202;
static u16   *const BIOS_IF = (void *)0x03FFFFF8; // not hardware but related
static void* *const BIOS_IV = (void *)0x03FFFFFC; // not hardware but related

// Definitions for the above registers
#define DC_MODE      0
#define DC_FRAME     3
#define DC_OAMHBLANK 5
#define DC_OBJ_1D    6
#define DC_BLANK     7
#define DC_BG0       8
#define DC_BG1       9
#define DC_BG2       10
#define DC_BG3       11
#define DC_OBJ       12
#define DC_WINDOW0   13
#define DC_WINDOW1   14
#define DC_OBJWINDOW 15

#define DS_VBLANK     0
#define DS_HBLANK     1
#define DS_VCOUNTER   2
#define DS_VBLANK_IE  3
#define DS_HBLANK_IE  4
#define DS_VCOUNT_IE  5
#define DS_VCOUNT     8

#define BGC_PRIORITY        0
#define BGC_PIXMAP_BLOCK    2
#define BGC_PIXMAP_ADDR_RSH 12
#define BGC_MOSAIC          6
#define BGC_PALETTE_256     7
#define BGC_MAP_BLOCK       8
#define BGC_MAP_ADDR_LSH    3
#define BGC_WRAPAROUND      13
#define BGC_SIZE_CODE       14
#define BGC_RSIZE_256X256   0  // regular BG sizes
#define BGC_RSIZE_512X256   1
#define BGC_RSIZE_256X512   2
#define BGC_RSIZE_512X512   3
#define BGC_ASIZE_128X128   0  // affine BG sizes
#define BGC_ASIZE_256X256   1
#define BGC_ASIZE_512X512   2
#define BGC_ASIZE_1024X1024 3

#define WI0_BG_ENABLE       0
#define WI0_OBJ_ENABLE      4
#define WI0_SFX_ENABLE      5
#define WI1_BG_ENABLE       8
#define WI1_OBJ_ENABLE      12
#define WI1_SFX_ENABLE      13

#define IE_LCD_VBLANK       0

#define PHYSSCR_WIDTH   240
#define PHYSSCR_HEIGHT  160
#define LOGSCR_WIDTH    128
#define LOGSCR_HEIGHT   128
// Logical screen's physical coordinates:
#define LOGSCR_PHLEFT   ((PHYSSCR_WIDTH/2)-(LOGSCR_WIDTH/2))
#define LOGSCR_PHTOP    ((PHYSSCR_HEIGHT/2)-(LOGSCR_HEIGHT/2))
#define LOGSCR_PHRIGHT  ((PHYSSCR_WIDTH/2)+(LOGSCR_WIDTH/2))
#define LOGSCR_PHBOTTOM ((PHYSSCR_HEIGHT/2)+(LOGSCR_HEIGHT/2))

// Framework imported variables
extern u16 fw__palette[];
extern u16 fw__tileset[];
extern u16 fw__spritesheet[];
extern s32 fw__mapWidth;
extern s32 fw__mapHeight;
extern u16 fw__tilemap[];

// Exported variables
volatile u32 fw__vsynccnt = 0;

// Limited resources:
// - Backgrounds (4) (some are bitmaps)
// - BG Palettes (16)
// - OBJ Palettes (16)
// - Sprites (128)
// - Transforms (32)
// - Windows (1)

typedef struct {
  u16 attr0;
  u16 attr1;
  u16 attr2;
  u16 padding;
} obj_struct;

typedef u16 paltype[16];

// Variables for next frame, once they are final
static paltype bg_pal[16];
static obj_struct sprdata[128];
static u16 bmmap[16][16];
static int activePals;
static u32 next_vsynccnt = 0;
static u16 bgofs[4][2];

// Current draw context
static struct {
  u32 page;
  u32 nBGs;
  u32 nBMs;
  u32 clearCol;
  u32 nPals;
  u32 curPal;
  u32 nSprites;
  int x, y;
  bool isBM;
  bool clearFlag;
} ctx;

// 2 pixels per byte
#define BITMAP_SIZE (128*128/2)

static u16 bitmaps[BITMAP_SIZE/sizeof(u16)];

// DEBUG
static int passpoint = 0;

#define BGCNT_VAL(bg)\
    (3-bg)<<BGC_PRIORITY\
  | 0xC000>>BGC_PIXMAP_ADDR_RSH\
  |  false<<BGC_MOSAIC\
  |  false<<BGC_PALETTE_256\
  |     bg<<BGC_MAP_BLOCK\
  |  false<<BGC_WRAPAROUND\
  | BGC_RSIZE_256X256<<BGC_SIZE_CODE
// Static part of REG_BGnCNT. The only variable part is the page.
static const u16 bgcnt[4] = {
  // BG 0, map block 0, priority 3
    BGCNT_VAL(0)
  // BG 1, map block 1, priority 2
  , BGCNT_VAL(1)
  // BG 2, map block 2, priority 1
  , BGCNT_VAL(2)
  // BG 3, map block 3, priority 0
  , BGCNT_VAL(3)
};
#undef BGCNT_VAL

// Table of width/height codes for OBJ Size and OBJ Shape
#define x 255 // x means invalid
static u8 whcodes[8][8] = {
  //  8   16   24   32   40   48   56   64    // width
    0x0, 0x1,   x, 0x5,   x,   x,   x,   x    // height 8
  , 0x2, 0x4,   x, 0x9,   x,   x,   x,   x    // height 16
  ,   x,   x,   x,   x,   x,   x,   x,   x    // height 24
  , 0x6, 0xA,   x, 0x8,   x,   x,   x, 0xD    // height 32
  ,   x,   x,   x,   x,   x,   x,   x,   x    // height 40
  ,   x,   x,   x,   x,   x,   x,   x,   x    // height 48
  ,   x,   x,   x,   x,   x,   x,   x,   x    // height 56
  ,   x,   x,   x, 0xE,   x,   x,   x, 0xC    // heicht 64
};
#undef x

#if RT_EXCEPTION
static void errFmtWait(const char *fmt, ...)
{
  va_list v;
  va_start(v, fmt);
  acsl_errFmtWait(fmt, v);
}
#else
#define errFmtWat(fmt, ...)
#endif

#define RESOURCE_LIMIT_BG "Too many backgrounds requested"
#define RESOURCE_LIMIT_PAL "Too many palettes requested"
#define RESOURCE_LIMIT_SPR "Too many sprites requested"
static void errResourceLimit(const char *msg)
{
  errFmtWait("    Resources exhausted:\n\n      %s", msg);
}

u32 fw__getMap(int x, int y)
{
  if (x >= 0 && x < fw__mapWidth && y >= 0 && y < fw__mapHeight)
    return fw__tilemap[y * fw__mapWidth + x];
  return 0;
}

void fw__setMap(int x, int y, u32 id)
{
  if (x >= 0 && x < fw__mapWidth && y >= 0 && y < fw__mapHeight)
  {
    fw__tilemap[y * fw__mapWidth + x] = id & 0xDFF;
  }
}

static void flushClear()
{
  if (ctx.clearFlag)
  {
    ctx.nBGs = ctx.nBMs = ctx.nSprites = 0;
    ctx.isBM = ctx.clearFlag = false;
    if (ctx.curPal != 0)
      memcpy(bg_pal, bg_pal + ctx.curPal, sizeof(bg_pal[0]));
    ctx.curPal = ctx.nPals = 0;
    uint src = (ctx.clearCol & 0x80) >> 3 | ctx.clearCol & 0xF;
    bg_pal[0][0] = fw__palette[src];
  }
}

static void flushBM()
{
  if (ctx.isBM)
  {
    ctx.isBM = false;
    ++ctx.nBGs;
  }
}

static void freezePalette()
{
  if (ctx.nPals >= 16)
  {
    errResourceLimit(RESOURCE_LIMIT_PAL);
    return;
  }
  ctx.curPal = fw__reusePalette(&bg_pal, ctx.nPals);
  if (ctx.curPal != ctx.nPals)
    return;
  if (++ctx.nPals < 16)
  {
    // Prepare next working palette
    memcpy(bg_pal + ctx.nPals, bg_pal + ctx.curPal, sizeof(bg_pal[0]));
  }
}

void fw__chgDrawPaletteColour(u8 dest, s32 orig)
{
  flushClear();
  uint src = (u32)orig << 24 >> 31 << 4 | (u32)orig << 28 >> 28;

  bg_pal[ctx.nPals][dest & 0xF] = fw__palette[src];
}

void fw__chgScrPaletteColour(u8 dest, s32 orig)
{
  //flushClear();
  uint src = (u32)orig << 24 >> 31 << 4 | (u32)orig << 28 >> 28;
  dest &= 0xF;
  u16 rgb = fw__palette[src];
  u16 *limit = PALRAM + dest + 16 * activePals;
  for (u16 *entry = PALRAM + dest; entry != limit; entry += 16)
    *entry = rgb;
}

void fw__chgNextPaletteColour(u8 dest, s32 orig)
{
  flushClear();
  uint src = (u32)orig << 24 >> 31 << 4 | (u32)orig << 28 >> 28;
  dest &= 0xF;
  u16 rgb = fw__palette[src];
  u16 *limit = &bg_pal[0][dest] + 16 * ctx.nPals;
  for (u16 *entry = &bg_pal[0][dest]; entry != limit; entry += 16)
    *entry = rgb;
}

void fw__resetPalette()
{
  if (ctx.nPals < 16)
    memcpy(bg_pal + ctx.nPals, fw__palette, sizeof(bg_pal[0]));
}

void fw__camera(int x, int y)
{
  ctx.x = x;
  ctx.y = y;
}

void fw__drawMap(int x, int y, u32 tx, u32 ty, u32 tw, u32 th)
{
  flushClear();
  flushBM();
  freezePalette();
  if (ctx.nBGs >= 4)
  {
    errFmtWait("Can't draw more than 4 BG layers");
    return;
  }
  fw__renderMap(tx, ty, tw, th, x - ctx.x, y - ctx.y,
    &BGVRAM[ctx.page][ctx.nBGs][0], &bgofs[ctx.nBGs][0], ctx.curPal);
  ++ctx.nBGs;
}

void fw__drawSprite(int x, int y, u16 tile, u8 width, u8 height, bool flipX, bool flipY)
{
  if (--width > 7 || --height > 7)
  {
    errFmtWait("Invalid width/height: %d,%d\nMaximum is 8,8", (u8)(width+1), (u8)(height+1));
    return;
  }
  u8 whcode = whcodes[height][width];
  if (whcode > 15)
  {
    errFmtWait(
      "Invalid sprite width/height: %d,%d\n"
      "Valid values are:\n"
      "    1,1     1,2     2,1     2,2\n"
      "    4,1     1,4     4,2     2,4\n"
      "    4,4     4,8     8,4     8,8");
    return;
  }

  flushClear();
  flushBM();
  freezePalette();

  if (ctx.nBGs == 0)
  {
    // Drawing sprites without having drawn a background is not supported.
    // Draw an empty background, to get priorities correct.
    fw__renderMap(0, 0, 0, 0, 0, 0, &BGVRAM[ctx.page][ctx.nBGs][0],
      &bgofs[ctx.nBGs][0], ctx.curPal);
    ++ctx.nBGs;
  }

  x += LOGSCR_PHLEFT - ctx.x;
  y += LOGSCR_PHTOP - ctx.y;
  int x2 = x + ++width  * 8;
  int y2 = y + ++height * 8;
  // Visibiliy test
  if (x2 <= LOGSCR_PHLEFT || x >= LOGSCR_PHRIGHT
      || y2 <= LOGSCR_PHTOP || y >= LOGSCR_PHBOTTOM)
    return;

  if (ctx.nSprites == 128)
  {
    errResourceLimit(RESOURCE_LIMIT_SPR);
    return;
  }
  obj_struct *spr = sprdata + ctx.nSprites++;
  spr->attr0 = (u16)y & 255 | whcode<<14;
  spr->attr1 = (u16)x & 511 | flipX<<12 | flipY<<13 | (whcode >> 2) << 14;
  spr->attr2 = tile & 0x1FF | (4 - ctx.nBGs) << 10 | ctx.curPal << 12;
}

void fw__clear(uint32_t n)
{
  ctx.clearFlag = true;
  ctx.clearCol = n;
}


void fw__present(void)
{
  for (uint i = 0; i < 4; ++i)
  {
    REG_BGCNT[i] = bgcnt[i] | ctx.page << (BGC_MAP_BLOCK + 2);
  }
  fw__memcpy32_small(PALRAM, bg_pal, ctx.nPals * sizeof(bg_pal[0]));
  fw__memcpy32_small(PALRAM+256, bg_pal, ctx.nPals * sizeof(bg_pal[0]));
  activePals = ctx.nPals;
  // Copy the sprites in reverse order to respect drawing priority
  fw__copySpritesReverse(sprdata, ctx.nSprites);
  // Disable rest of sprites
  for (u16 *p = OAM + ctx.nSprites * 4; p != OAM + 128*4; p += 4)
    *p = 0x0200;
  // Copy offset data
  fw__memcpy32_small(REG_BGOFS, bgofs, sizeof(u16) * 2 * ctx.nBGs);
  // Enable planes
  *REG_DISPCNT
    =     0<<DC_MODE
    |     0<<DC_FRAME
    | false<<DC_OAMHBLANK
    | false<<DC_OBJ_1D
    | false<<DC_BLANK
    |  true<<DC_OBJ
    |  true<<DC_WINDOW0
    | false<<DC_WINDOW1
    | false<<DC_OBJWINDOW
    | ((1 << ctx.nBGs) - 1) << DC_BG0;
    ;
  ctx.page = 1 - ctx.page;
}

bool fw__secondVSync()
{
  u32 freeze = fw__vsynccnt;
  if (__builtin_sub_overflow_p(next_vsynccnt, freeze, freeze))
  {
    next_vsynccnt = freeze + 1;
    return true;
  }
  return false;
}

static void setInts()
{
  *REG_IME = 0;
  *BIOS_IV = &fw__intHandler;
  *REG_IE = 1<<IE_LCD_VBLANK;
  *REG_IME = 1;
  *REG_DISPSTAT |= 1<<DS_VBLANK_IE;
/*
//  Probably not our business...
  asm (
    " bx pc\n"
    " .balign 4\n"
    " .arm\n"
    " mrs r0, cpsr\n"
    " bic r0, #128\n" // clear I flag in CPSR
    " msr cpsr, r0\n"
    " add lr, pc, #1\n"
    " bx lr\n"
    " .thumb\n"
    );
*/
}

void fw__init()
{
  memcpy(bg_pal, fw__palette, sizeof(bg_pal[0]));
  memset(VRAM, 0, 0x18000);
  memset(PALRAM, 0, 16*16*2*sizeof(u16));
  memcpy(VRAM+0x0C000/sizeof(u16), fw__tileset, 256*128/2);
  memcpy(VRAM+0x10000/sizeof(u16), fw__spritesheet, 256*128/2);
  for (u16 y = 0; y < 16; y++)
    for (u16 x = 0; x < 16; x++)
      bmmap[y][x] = (y<<4) + x;

  // Disable all sprites
  for (u16 *p = OAM; p != OAM + 128*4; p += 4)
    *p = 0x200;
  // Clear all padding words in sprite memory buffer
  for (u32 i = 0; i != 128; ++i)
    sprdata[i].padding = 0;

  REG_WINH[0] = (240/2-128/2) << 8 | (240/2+128/2);
  REG_WINV[0] = (160/2-128/2) << 8 | (160/2+128/2);
  *REG_WININ  = ((1<<4)-1)<<WI0_BG_ENABLE
    |  true<<WI0_OBJ_ENABLE
    | false<<WI0_SFX_ENABLE
    |     0<<WI1_BG_ENABLE
    | false<<WI1_OBJ_ENABLE
    | false<<WI1_SFX_ENABLE
    ;
  *REG_WINOUT = 0; // all black outside window

  ctx.page = 0;
  fw__clear(0);
  flushClear();
  setInts();
  next_vsynccnt = fw__vsynccnt;
  fw__present();
}
