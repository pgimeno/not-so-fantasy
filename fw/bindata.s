/*
 * RetroAdvance framework - Import graphics and palette as binary
 *
 * (C) Copyright 2021 Pedro Gimeno Fortea
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
		.syntax	unified
		.cpu	arm7tdmi

		.global	fw__palette
		.global	fw__tileset
		.global	fw__spritesheet
		.global	fw__tilemap
		.global	fw__mapWidth
		.global	fw__mapHeight

		.section .rodata

		.balign	4
fw__palette:	.incbin	"palette.bin"
		.incbin "palette2.bin"

fw__tileset:	.incbin	"tileset.bin"

fw__spritesheet:
		.incbin	"spritesheet.bin"

fw__mapWidth	= . + 0
fw__mapHeight	= . + 4
		.incbin	"mapsize.bin"

		.section .data
fw__tilemap	= .
		.incbin	"tilemap.bin"
