--[[

 RetroAdvance framework - LIKO-12 .lk12 file to TGA converter

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

local f = io.open(arg[1], "rb")
repeat
  local line = f:read("*l")
  if line then
    if line == "___spritesheet___" then
      line = f:read("*l")
      assert(line == "LK12;GPUIMG;192x128;")
      f2 = io.open(arg[2], "wb")
      f2:write(
  "\x00\x01\x01\x00\x00\x10\x00\x18\x00\x00\x80\x00\x00\x01\x80\x00\x08\x20"
.."\x00\x00\x00\x50\x28\x20\x50\x28\x80\x50\x88\x00\x38\x50\xA8\x50"
.."\x58\x60\xC8\xC0\xC0\xE0\xF0\xF8\x48\x00\xF8\x00\xA0\xF8\x28\xE8"
.."\xF8\x38\xE0\x00\xF8\xA8\x28\x98\x78\x80\xA8\x78\xF8\xA0\xC8\xF8")

      local hex = {'1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'}
      local rep = {}
      for i = 1, 15 do rep[i] = string.char(i) end
      repeat
        line = f:read("*l")
        if line:byte(1) == 59 then -- 59 = ";"
          break
        end
        line = line:gsub("0", "\0")
        for i = 1, 15 do
          line = line:gsub(hex[i], rep[i])
        end
        f2:write(line, ("\0"):rep(64))
      until false
      f2:close()
      break
    end
  end
until not line
f:close()
