--[[

 RetroAdvance framework - LIKO-12 .lk12 file to binary tilemap converter

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

local f = io.open(arg[1], "r")
repeat
  local line = f:read("*l")
  if line then
    if line == "___tilemap___" then
      line = f:read("*l")
      assert(line == "LK12;TILEMAP;144x128;")
      if #arg < 3 then error("needs 3 arguments") end
      local f2 = io.open(arg[3], "wb")
      f2:write('\144\0\0\0\128\0\0\0')
      f2:close()
      f2 = io.open(arg[2], "wb")
      local cnt = 0
      repeat
        line = f:read("*l")
        cnt = cnt + 1
        for i in line:gmatch("[^;]+") do
          local a = tonumber(i)-1
          if a ~= -1 then
            -- compensate for the extra 8 tiles per row
            a = a % 24 + math.floor(a/24)*32
            f2:write(string.char(a%256), string.char(math.floor(a/256)))
          else
            f2:write('\255\1')
          end
        end
      until cnt == 128
      f2:close()
      break
    end
  end
until not line
f:close()
