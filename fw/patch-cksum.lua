--[[

 RetroAdvance framework - GBA Header Checksum Patcher

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

local f = io.open(arg[1], "r+b")
assert(f, "File not found, read only, or invalid command line")
local hdr = f:read(0xBD)
assert(#hdr == 0xBD and hdr:sub(5, 8) == "\36\255\174\81"
       and hdr:byte(0xB2) ~= 0x96, "Invalid file format, not a GBA raw ROM")

local sum = 0xE7
for i = 0xA1, 0xBD do
  sum = (sum - hdr:byte(i)) % 256
end
f:write(string.char(sum))
