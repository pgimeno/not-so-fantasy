/*
 * RetroAdvance framework for GBA - Assembler routines
 *
 * (C) Copyright 2021 Pedro Gimeno Fortea
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
		.syntax	unified
		.cpu	arm7tdmi

		.global	fw__intHandler
		.global	fw__waitVSyncInt
		.global	fw__memcpy32_small
		.global	fw__copySpritesReverse
		.global	fw__reusePalette
		.global	fw__renderMap

		.section .text.ARM
		.arm

SWI_THUMB	= 0
SWI_ARM		= 16

INT_VBLANK	= 0
INT_HBLANK	= 1
INT_VCOUNT	= 2
INT_TIMER0	= 3
INT_TIMER1	= 4
INT_TIMER2	= 5
INT_TIMER3	= 6
INT_SERIAL	= 7
INT_DMA0	= 8
INT_DMA1	= 9
INT_DMA2	= 10
INT_DMA3	= 11
INT_KEYPAD	= 12
INT_EXTERNAL	= 13

fw__intHandler:	mov	r0,0x04000000
		ldrh	r1,[r0,-8]		@ BIOS_IF
		orr	r1,1<<INT_VBLANK
		strh	r1,[r0,-8]
		add	r0,0x200		@ REG_IE
		mov	r1,0			@ master disable ints
		strh	r1,[r0,0x04]		@ REG_IME
		ldr	r2,[r0,0x02]		@ REG_IF (grab snapshot)
		mov	r1,1<<INT_VBLANK	@ ack int
		strh	r1,[r0,0x02]		@ REG_IF
		mov	r1,1			@ master enable ints
		strh	r1,[r0,0x04]		@ REG_IME

		@ We're not here just for SWI 5 :)
		ldr	r0,=fw__vsynccnt
		ldr	r1,[r0]
		add	r1,1
		str	r1,[r0]

		mov	pc,lr

		.ltorg

		.section .text
		.thumb

fw__waitVSyncInt:
		swi	5 << SWI_THUMB
		bx	lr

		.section .text.ARM
		.arm

@ void fw__memcpy32_small(char *restrict d, const char *restrict s, size_t n)
fw__memcpy32_small:
		cmp	r2,4
		bxlo	lr
		mov	r12,r0
1:		ldr	r3,[r1],4
		str	r3,[r12],4
		subs	r2,4
		bhi	1b
		bx	lr

@ void fw__copySpritesReverse(const void *src, size_t n);
fw__copySpritesReverse:
		cmp	r1,0
		bxeq	lr
		mov	r12,0x07000000	@ OAM
		add	r12,r12,r1,lsl 3
1:		ldmia	r0!,{r2,r3}
		strh	r3,[r12,-4]!
		stmdb	r12!,{r2}
		subs	r1,1
		bne	1b
		bx	lr

@ int fw__reusePalette(void *bg_pal, int nPals);
@ returns nPals if unique, else duplicate number
fw__reusePalette:
		add	r12,r0,r1,lsl 5		@ R12 = &bg_pal[nPals][0]
		cmp	r1,0
		mov	r1,r0
		mov	r0,0			@ also return 0 if nPals=0
		bxeq	lr

		push	{r4-r10}
		ldmia	r12,{r3-r10}		@ load reference palette
1:		ldr	r2,[r1]			@ compare one by one
		cmp	r2,r3
		bne	2f			@ if any of them differs, jump
		ldr	r2,[r1,4]
		cmp	r2,r4
		bne	2f
		ldr	r2,[r1,8]
		cmp	r2,r5
		bne	2f
		ldr	r2,[r1,12]
		cmp	r2,r6
		bne	2f
		ldr	r2,[r1,16]
		cmp	r2,r7
		bne	2f
		ldr	r2,[r1,20]
		cmp	r2,r8
		bne	2f
		ldr	r2,[r1,24]
		cmp	r2,r9
		bne	2f
		ldr	r2,[r1,28]
		cmp	r2,r10
		bne	2f
		@ All equal - we've found a match; return R0
		pop	{r4-r10}
		bx	lr

2:		add	r0,1		@ next palette index
		add	r1,32		@ next palette pointer
		cmp	r1,r12		@ check limit
		blo	1b		@ loop while limit not hit
		pop	{r4-r10}
		bx	lr		@ return R0 = nPals


@ void fw__renderMap(u32 tx, u32 ty, u32 tw, u32 th, int x, int y, u16 *bgmap,
@   u32 *bgofs, u32 pal);
fw__renderMap:	push	{r4-r11,lr}
		nregspushed = 9

tx		.req	r0
ty		.req	r1
tx2		.req	r2		@ actually tw, but we set it to tx+tw
ty2		.req	r3		@ early, and same with ty/th/ty2
mapw		.req	r4
maph		.req	r5

		ldr	r4,=fw__mapWidth
		ldmia	r4,{mapw,maph}

		add	tx2,tx
		cmp	tx2,mapw	@ if tx2 > mapWidth:
		movhi	tx2,mapw	@   tx2 := mapWidth
		add	ty2,ty
		cmp	ty2,maph
		movhi	ty2,maph
		.unreq	maph		@ maph no longer needed, r5 free
		sub	tx2,1		@ point to last tile, not past it
		sub	ty2,1

tmp		.req	r12
pmap		.req	lr

		@ In map coordinates, the screen rectangle is:
		@ [tx*8-x,ty*8-y,128,128]
		@ 1) Transform that to maptile coordinates.
		@ 2) Iterate over the result, and when the coordinate is
		@ outside the map, replace the map entry with 0.

		add	r5,sp,nregspushed*4
		ldmia	r5,{r5-r8,r11}
x		.req	r5
y		.req	r6
pbgmap		.req	r7
pbgofs		.req	r8
pal		.req	r11

		neg	tmp,x
		and	tmp,0x7		@ *pbgofs.W = 512 - x % 8
		add	tmp,512-56
		neg	r10,y
		and	r10,0x7		@ *pbgofs.H = 512 - y % 8
		add	r10,512-16
		orr	tmp,tmp,r10,lsl 16
		str	tmp,[pbgofs]	@ store both at once
		.unreq	pbgofs		@ r8 now free again

		rsb	x,x,tx,lsl 3
		rsb	y,y,ty,lsl 3
x2		.req	r8
y2		.req	r9		@ r8,r9 in use
		add	x2,x,128+7	@ +7 for ceil
		add	y2,y,128+7
		asr	x,3		@ floor(x/8)
		asr	y,3		@ floor(y/8)
		asr	x2,3		@ ceil(x2/8)
		asr	y2,3		@ ceil(y2/8)

		@ Set pmap = starting map offset
		mul	pmap,mapw,y
		add	pmap,x
		ldr	tmp,=fw__tilemap
		add	pmap,tmp,pmap,lsl 1

xsave		.req	r10
		mov	xsave,x
1:		mov	x,xsave
		push	{y2,pmap}	@ we're out of regs so we reuse y2
		mov	y2,512
		sub	y2,1		@ last tile = 511
2:		cmp	x,tx		@ if x >= tx
		cmpge	tx2,x		@ and x <= tx2
		cmpge	y,ty		@ and y >= ty
		cmpge	ty2,y		@ and y <= ty2
		ldrhge	tmp,[pmap]	@ then load tile from map
		orrge	tmp,tmp,pal,lsl 12	@ (apply palette too if so)
		movlt	tmp,y2		@ else tile = 511
		strh	tmp,[pbgmap],2	@ *pbgmap++ = tile
		add	pmap,2		@ advance map pointer always
		add	x,1		@ next x
		cmp	x,x2		@ check if right of screen reached
		blt	2b		@ loop if not

		sub	x,xsave
		rsb	x,32
		add	pbgmap,pbgmap,x,lsl 1	@ add offset to next
		pop	{y2,pmap}
		add	pmap,pmap,mapw,lsl 1
		add	y,1
		cmp	y,y2		@ check if bottom of screen reached
		blt	1b		@ loop if not

ret:		pop	{r4-r11,lr}
		bx	lr

		.unreq	tx		@ r0
		.unreq	ty		@ r1
		.unreq	tx2		@ r2
		.unreq	ty2		@ r3
		.unreq	mapw		@ r4
		.unreq	x		@ r5
		.unreq	y		@ r6
		.unreq	pbgmap		@ r7
		.unreq	x2		@ r8
		.unreq	y2		@ r9
		.unreq	xsave		@ r10
		.unreq	pal		@ r11
		.unreq	tmp		@ r12
		.unreq	pmap		@ lr

		.ltorg
