--[[

 RetroAdvance framework - LIKO-12 .lk12 file to Tiled .tmx converter

 (C) Copyright 2021 Pedro Gimeno Fortea

 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.

 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

--]]

local f = io.open(arg[1], "r")
repeat
  local line = f:read("*l")
  if line then
    if line == "___tilemap___" then
      line = f:read("*l")
      assert(line == "LK12;TILEMAP;144x128;")
      f2 = io.open(arg[2], "w")
      f2:write('<?xml version="1.0" encoding="UTF-8"?>\n'
..'<map version="1.0" orientation="orthogonal" renderorder="right-down" width="144" height="128" tilewidth="8" tileheight="8" nextobjectid="1">\n'
..' <tileset firstgid="1" name="tileset" tilewidth="8" tileheight="8" tilecount="512" columns="32">\n'
..'  <image source="tileset.png" width="256" height="128"/>\n'
..' </tileset>\n'
..' <layer name="Tile Layer 1" width="144" height="128">\n'
..'  <data encoding="csv">\n'
)
      local cnt = 0
      local t = {}
      repeat
        line = f:read("*l")
        cnt = cnt + 1
        local elem = 0
        for i in line:gmatch("[^;]+") do
          local a = tonumber(i)-1
          if a ~= -1 then
            a = a % 24 + math.floor(a/24)*32
          end
          elem = elem + 1
          t[elem] = tostring(a+1)
        end
        if cnt == 128 then
          t[elem+1] = nil
        else
          t[elem+1] = ""
        end
        f2:write(table.concat(t, ","), "\n")
      until cnt == 128
      f2:write('</data>\n </layer>\n</map>\n')
      f2:close()
      break
    end
  end
until not line
f:close()
