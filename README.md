# RetroAdvance - GBA retro-dev framework

This framework is inspired by the PICO-8 fantasy console, but it uses a real non-fantasy console, namely Nintendo's Game Boy Advance.

This is work in progress.

## Development Blog

https://codeberg.org/pgimeno/not-so-fantasy-blog/issues

## Status

This is still in early stages of development, but some basic API functions are working:

- btn(), spr(), map(), mget(), mset(), pal(), cls(), camera(), flip() all work. Note that pal() works slightly differently to how it works on PICO-8, in that it can change a colour to an alternative one (from the set -16..-1), making it able to display 32 colours at the same time. The third parameter of `pal()` might be working but is not tested. A third parameter of 1 is the only one tested so far.
- Note that cls(n) (or mapping colour 0 of the initial palette via pal()) changes the colour of the whole 240x160 screen, not just the 128x128 drawable screen. I haven't found a way around this, other than wasting a map layer just for the background colour.
- The _update(), _update60() and _draw() events are working too.

There's no sound at all yet.

## Installation

When this project is finished, installation will be automated, but for now, it requires manual installation of dependencies.

First you need to install GNU make and the ARM GCC compiler. If your system is ARM-based, your regular compiler will probably work; if not, you need a cross-compiler. Install instructions for those packages are not provided here at this moment, sorry. For Debian/Ubuntu on non-ARM, you can just install the packages `make`, `gcc-arm-none-eabi` and `binutils-arm-none-eabi`.

Once you have them installed and working, just you need three packages (Nelua, ACSL and RetroAdvance) in the right places:

- Create a directory for the RetroAdvance toolset, naming it as you prefer. It will contain a subdirectory for Nelua, another for ACSL and another for the RetroAdvance framework itself.
- Unpack [Nelua](https://github.com/edubart/nelua-lang/releases) inside the directory created above. Rename the folder that was created (the one that ends with `-x86_64-latest`) to `nelua-lang`. Alternatively, if you want/need to compile from source, clone the Nelua repository to nelua-lang, then compile it there.
- Clone or unpack [ACSL](https://codeberg.org/pgimeno/acsl) to the subdirectory `acsl` inside that directory. Follow the compilation instructions (which basically boil down to running 'make' in the ACSL directory).
- Clone or unpack RetroAdvance (this repository) as a subdirectory of the main toolset directory.

For example, this is a possible resulting folder structure (where each subdirectory will contain more subdirectories in turn, but these are not shown):

- `ra` (for example - toolset directory)
  - `acsl` (standard C library for GBA)
  - `nelua-lang` (Nelua itself)
  - `retroadvance` (the RetroAdvance framework, this repository)

That should do it. More advanced users can choose to use a different folder for Nelua/ACSL and edit `Makefile` or pass appropriate variables to `make`.

You may need to customize some options for `make` to work with your system:

- The Makefile expects the ARM toolchain to be available in the path, and to be called `arm-none-eabi-*` where `*` stands for the name of every tool needed: `gcc`, `as`, `ld`, `gcc-ar`, `objcopy`. If the prefix of these tools is different, you need to alter the make variable `ARM` accordingly. If they are all in a common directory that is not in the path, you can specify the whole path there.
- The framework needs a Lua 5.3+ interpreter for some build tools and converters to work. Nelua comes with one already bundled that RetroAdvance can use. Normally the Lua executable is in `../nelua-lang/nelua-lua`; if it's different, you may want to alter the make variable `LUA` accordingly.
- The build system expects the ACSL library to be located in `../acsl/`; if that's not the case you need to alter the make variable `ACSL_PATH` accordingly.

## Usage

In the directory `retroadvance`, create a folder named `projects` (lower case, for case-sensitive filesystems). Inside that folder, create a subfolder with your desired project name.

This subfolder needs to contain the following files:

- `main.nelua` - This is where the code of your program needs to be placed. It may require other code files in turn.
- `tileset.tga` - Contains the graphical data for the map tiles. This needs to be a Truevision Targa (TGA) image with the following characteristics:
  - Not compressed.
  - Top-down.
  - With a 16 colour palette. This will be the primary palette used by the program.
  - Resolution 256x128.
  - No alpha channel.
  - Structured as a set of 8x8 pixel tiles, i.e. 32x16 tiles. The last tile (the one at the bottom right) must be filled with colour 0 from the palette.
  - Note that pixels with the colour index 0 will be transparent when rendered by the GBA.
- `spritesheet.tga` - Same characteristics as `tileset.tga`, but this one is used for sprites only. If you don't want different tiles for sprites and map, you can just copy `tileset.tga` as `spritesheet.tga`.
- `tilemap.lua` - This is the result of exporting a [Tiled](https://www.mapeditor.org/) map to Lua.
  - Note that at least the Linux version of Tiled does not support indexed TGA images for tilesets, so you may need to export the tileset to PNG in addition to TGA in order to see it in Tiled.
  - If you don't have Tiled and want to create the file manually, the required format is a Lua file with this structure: `return {width = <width>, height = <height>, layers = {{data = {...}}}}` where the data is a flat array of `<width>*<height>` entries each from 0 to 512, where 0 is equivalent to 512, 1 is the top left tile of the tileset image and 512 is the bottom right tile (which must always be empty). Each value in the data array represents a tile in the map, with `<width>` tiles left-to-right, then `<height>` tiles top-to-bottom.
  - Lua tools are included that allow conversion from [LIKO-12](https://ramilego4game.itch.io/liko12) maps to Tiled maps, and from LIKO-12 tileset images to TGA. These tools have no documentation yet, sorry. LIKO-12 is another fantasy console inspired by PICO-8, that runs under the [Löve](https://love2d.org/) framework, and can be considered an alternative to PICO-8.
- `palette2.bin` - Optional. If it exists, it will be used instead of the default one as a second palette. The format is the one required by the GBA: sixteen two-byte entries in little-endian order, with the colours as XBBBBBGGGGGRRRRR in binary (where X is don't care and R, G, B are bits, 5 per component). The included Lua tools can extract one from a TGA file, but again there's no documentation yet.

It doesn't hurt to have more files, of course. For example, you may want to save the Tiled `tilemap.tmx` map and the GIMP `tileset.xcf` project file in that folder, and you may want to split your project into multiple Nelua files; that's up to you, but better don't use the `out/` subdirectory because it will be wiped by `make clean`.

There is an image template in the `fw/` subdirectory of this repository that you can use as guide. The .xcf.bz2 file is for GIMP; you can enable the grid in order to see the tiles, using View > Show Grid. The same file is also in .tga format.

With all these things in the right folders, you just have to invoke GNU Make from the `retroadvance` subfolder, setting the `PRJ` variable to the name of the project (the same name you used for the subfolder in `retroadvance/projects/`). For example:

    cd ~/ra/retroadvance
    make PRJ=MyGame

will compile the project in `retroadvance/projects/MyGame/`. If everything goes right, that should create a subdirectory called `out` inside your project folder, and in that subdirectory there should be, among other subproducts of compilation, a file called `<your project name>.gba` with the generated ROM; following this example, the file will be `retroadvance/projects/MyGame/out/MyGame.gba`.

## Limitations

The GBA hardware imposes some limits to hardware accelerated graphics rendering, which this framework attempts to fully use in order to achieve a reasonable speed. This means that the GBA limitations apply to the framework as well:

- Maximum 128 sprites.
- Colour index 0 of tiles and sprites is transparent. This can't be changed, therefore there won't be a `palt()` function.
- Maximum 4 maps. Note that due to a technical limitation, drawing a sprite without drawing any map first uses a map slot. Drawing in bitmap mode also uses up one map.
- Some sprites might not be rendered under certain conditions (big sprites, many maps, many sprites on the same line).
- Maximum 16 palette slots. When `map()` or `spr()` are called, the current palette, if it doesn't repeat, uses up one palette slot (if it repeats, i.e. if it is equal to one used before, then the previous one is reused). The palette is changed via the `pal()` API call.
- (Future): 32 transformations, i.e. sspr() calls with different parameters.
- (Future): 1 clip() call, or maybe none. That needs to be checked (it will use the GBA window function; the GBA has two windows, and one is already in use for clipping graphics to 128x128).

You can read https://codeberg.org/pgimeno/not-so-fantasy-blog/issues/3 for an outline on the technical approach and how it will impose limits to what's possible.

## License

The license is ISC. See the file LICENSE.md for details.
